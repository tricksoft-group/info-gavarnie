const app = require('express')();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const Datastore = require('nedb');
const fs = require('fs');
const ObjectsToCsv = require('node-create-csv');
const parser = require('csvtojson');
const atop = require('atob');
const csv = process.env.APPDATA + '/POS/exports/data.csv';

app.use(bodyParser.json());

module.exports = app;

let dataDB = new Datastore({
	filename: process.env.APPDATA + '/POS/server/databases/data.db',
	autoload: true,
});

dataDB.ensureIndex({ fieldName: 'email', unique: true });
//console.log(csv);
//setInterval(() => {
parser()
	.fromFile(csv)
	.then((jsonObj) => {
		//console.log(jsonObj);
		for (let i in jsonObj) {
			if (jsonObj[i].email != '') {
				dataDB.findOne(
					{
						email: jsonObj[i].email,
					},
					function (err, data) {
						delete jsonObj[i]._id;
						console.log(err, data);
						if (typeof data != 'undefined' && data != null) {
							Object.assign(data, jsonObj[i] || {});

							dataDB.update(
								{
									email: jsonObj[i].email,
								},
								data,
								{},
								function (err, numReplaced, data) {
									console.log(numReplaced + ' row replaced', err, data);
								},
							);
						} else {
							console.log('1 row added', jsonObj[i]);
							dataDB.insert(jsonObj[i]);
						}
					},
				);
			}
		}
	});
//}, 6000);

app.get('/', function (req, res) {
	res.send('Data API');
});

function saveSpecificCsv(docs, field, allColumns) {
	let datas = [];
	allColumns = typeof allColumns != 'undefined' ? allColumns : false;
	new Promise((resolve) => {
		let data = docs;
		if (typeof docs == 'string') {
			data = JSON.parse(docs);
		}
		return resolve(data);
	})
		.then((data) => {
			data.forEach((doc) => {
				let parsed = {};
				parsed.show = doc.from;
				parsed.name = (doc.first_name ?? '') + (' ' + doc.name ?? '');
				if (typeof doc[field] != 'undefined') {
					try {
						parsed = Object.assign(parsed, JSON.parse(doc[field]) || {});
						datas.push(parsed);
					} catch (error) {
						//console.error(error);
					}
				}
			});
		})
		.then(() => {
			let csvd = new ObjectsToCsv(datas);
			csvd.toDisk(process.env.APPDATA + '/POS/exports/' + field + '.csv', {
				showHeader: true,
				allColumns: true,
			});
			console.log(process.env.APPDATA + '/POS/exports/' + field + '.csv updated!');
		});
}

setInterval(() => {
	dataDB.find({}, function (err, docs) {
		const csv = new ObjectsToCsv(docs);

		// Save to file:
		csv.toDisk(process.env.APPDATA + '/POS/exports/data.csv', {
			showHeader: true,
			allColumns: true,
		});
		console.log(process.env.APPDATA + '/POS/exports/data.csv updated!');
		saveSpecificCsv(docs, 'dates_provided');
		saveSpecificCsv(docs, 'breakfast_number');
		saveSpecificCsv(docs, 'lunch_number');
		saveSpecificCsv(docs, 'extra_lunch_number');
		saveSpecificCsv(docs, 'feeding', true);
		saveSpecificCsv(docs, 'extra_dinner_number');
		saveSpecificCsv(docs, 'dinner_number');
		saveSpecificCsv(docs, 'allergy');
	});
}, 10000);

app.put('/data', function (req, res) {
	dataDB.findOne(
		{
			email: req.body.email,
		},
		function (err, data) {
			delete req.body.language;
			delete req.body.endpoint;

			if (typeof data != 'undefined' && data != null) {
				new Promise((resolve) => {
					Object.assign(data, req.body || {});
					resolve();
				}).then(() => {
					dataDB.update(
						{
							email: req.body.email,
						},
						data,
						{},
						function (err, numReplaced, data) {
							console.log(numReplaced + ' row replaced', err, data);

							if (err) res.status(500).send(err);
							else res.sendStatus(200);
						},
					);
				});
			} else {
				dataDB.insert(req.body);
				res.sendStatus(200);
			}
		},
	);
});

app.get('/data/:email', function (req, res) {
	if (!req.params.email) {
		res.status(500).send('Id field is required.');
	} else {
		let email = req.params.email;

		dataDB.findOne(
			{
				email: email,
			},
			function (err, data) {
				console.log(data, err);
				if (err) res.status(500).send(err);
				else res.send(data);
			},
		);
	}
});
