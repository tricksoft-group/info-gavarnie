const app = require('express')();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const Datastore = require('nedb');
const fs = require('fs');
const ObjectsToCsv = require('node-create-csv');
const parser = require('csvtojson');
const atop = require('atob');
const csv = process.env.APPDATA + '/POS/exports/lunch.csv';

app.use(bodyParser.json());

module.exports = app;

let lunchDB = new Datastore({
	filename: process.env.APPDATA + '/POS/server/databases/lunch.db',
	autoload: true,
});

lunchDB.ensureIndex({ fieldName: 'email', unique: true });
//console.log(csv);
//setInterval(() => {
parser()
	.fromFile(csv)
	.then((jsonObj) => {
		//console.log(jsonObj);
		for (let i in jsonObj) {
			if (jsonObj[i].email != '') {
				lunchDB.findOne(
					{
						email: jsonObj[i].email,
					},
					function (err, data) {
						delete jsonObj[i]._id;
						console.log(err, data);
						if (typeof data != 'undefined' && data != null) {
							Object.assign(data, jsonObj[i] || {});

							lunchDB.update(
								{
									email: jsonObj[i].email,
								},
								data,
								{},
								function (err, numReplaced, data) {
									console.log(numReplaced + ' row replaced', err, data);
								},
							);
						} else {
							console.log('1 row added', jsonObj[i]);
							lunchDB.insert(jsonObj[i]);
						}
					},
				);
			}
		}
	});
//}, 6000);
app.get('/', function (req, res) {
	res.send('Lunch API');
});

function saveSpecificCsv(docs, field, allColumns) {
	let datas = [];
	allColumns = typeof allColumns != 'undefined' ? allColumns : false;
	new Promise((resolve) => {
		docs.forEach((doc) => {
			let parsed = {};
			parsed.date = doc.date;
			parsed.name = (doc.first_name ?? '') + (' ' + doc.name ?? '');
			if (typeof doc[field] != 'undefined') {
				try {
					parsed = Object.assign(parsed, JSON.parse(doc[field]) || {});
					datas.push(parsed);
				} catch (error) {
					//console.error(error);
				}
			}
		});

		resolve(true);
	}).then(() => {
		let csvd = new ObjectsToCsv(datas);
		csvd.toDisk(process.env.APPDATA + '/POS/exports/' + field + '.csv', {
			showHeader: true,
			allColumns: true,
		});
		console.log(process.env.APPDATA + '/POS/exports/' + field + '.csv updated!');
	});
}

setInterval(() => {
	lunchDB.find({}, function (err, docs) {
		const csv = new ObjectsToCsv(docs);

		// Save to file:
		csv.toDisk(process.env.APPDATA + '/POS/exports/lunch.csv', {
			showHeader: true,
		});
		saveSpecificCsv(docs, 'food', true);
		saveSpecificCsv(docs, 'food_share', true);
		saveSpecificCsv(docs, 'desserts', true);
		saveSpecificCsv(docs, 'drinks', true);
		saveSpecificCsv(docs, 'time', true);
	});
}, 10000);

app.put('/', function (req, res) {
	lunchDB.findOne(
		{
			email: req.body.email,
		},
		function (err, data) {
			delete req.body.language;
			delete req.body.endpoint;
			delete req.body.firstQuestion;
			delete req.body.standalone;

			if (typeof data != 'undefined' && data != null) {
				new Promise((resolve) => {
					Object.assign(data, req.body || {});
					resolve();
				}).then(() => {
					lunchDB.update(
						{
							email: req.body.email,
						},
						data,
						{},
						function (err, numReplaced, data) {
							console.log(numReplaced + ' row replaced', err, data);

							if (err) res.status(500).send(err);
							else res.sendStatus(200);
						},
					);
				});
			} else {
				lunchDB.insert(req.body);
				res.sendStatus(200);
			}
		},
	);
});

app.get('/:email', function (req, res) {
	if (!req.params.email) {
		res.status(500).send('Id field is required.');
	} else {
		let email = req.params.email;

		lunchDB.findOne(
			{
				email: email,
			},
			function (err, data) {
				console.log(data, err);
				if (err) res.status(500).send(err);
				else res.send(data);
			},
		);
	}
});
