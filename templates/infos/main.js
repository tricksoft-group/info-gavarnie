/*
  Copyright (c) 2020 - present, DITDOT Ltd. - MIT Licence
  https://github.com/ditdot-dev/vue-flow-form
  https://www.ditdot.hr/en
*/

import { createApp } from 'vue';
import Main from './main.vue';

createApp(Main).mount('#app');
