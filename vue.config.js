let entry = process.argv.pop();
if (entry && entry.indexOf('.js') === -1) {
	entry = null;
}

module.exports = {
	publicPath: '',
	pages: {
		index: {
			// Replace with your .js entry file path.
			// To see the quiz example, use 'examples/quiz/main.js'
			// To see the support page example, use 'examples/support-page/main.js'
			entry: entry || 'templates/index.js',
			template: 'public/index.html',
			filename: 'index.html',
		},
		infos: {
			// Replace with your .js entry file path.
			// To see the quiz example, use 'examples/quiz/main.js'
			// To see the support page example, use 'examples/support-page/main.js'
			entry: entry || 'templates/infos/main.js',
			template: 'public/index.html',
			filename: 'infos.html',
		},
		lunch: {
			// Replace with your .js entry file path.
			// To see the quiz example, use 'templates/quiz/main.js'
			// To see the support page example, use 'templates/support-page/main.js'
			entry: entry || 'templates/lunch/main.js',
			template: 'public/index.html',
			filename: 'lunch.html',
		},
	},
};
