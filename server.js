let express = require('express');
let http = require('http');
const path = require('path');
let bodyParser = require('body-parser');
let api = express();
let serverApi = http.createServer(api);

const PORT = process.env.PORT || 8001;

console.log('Server started');
api.use(bodyParser.json());
api.use(bodyParser.urlencoded({ extended: false }));

api.all('/*', function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	if (req.method == 'OPTIONS') {
		res.status(200).end();
	} else {
		next();
	}
});

api.use('/api/lunch', require('./api/lunch'));
api.use('/api', require('./api/data'));

serverApi.listen({ port: PORT, host: '0.0.0.0' }, () =>
	console.log(`Api Server Listening on PORT ${PORT}`),
);
